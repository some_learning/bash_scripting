#!/bin/bash

file=$1
dest=$2
shift
shift
rest=$@
if [ -z "$file" -o -z "$dest" -o -n "$rest" ]; then
	echo "Usage: $0 [file] [server]"
	exit 1
fi
read -s -p "Enter Password: " sudopassword
echo""
#temp=$(mktemp)

(echo $sudopassword;tar -cpz $file)| ssh $dest "sudo -p '' -S tar -xzp --no-same-owner -C /usr/local/bin $file"

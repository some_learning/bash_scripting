#!/bin/bash

if [ $# -gt 0 ]
then
	mynum=$1
else
	mynum=200
fi


if [ $mynum -eq 200 ]
then
	echo "The variable equals 200."
else [ $mynum -ne 200 ]
	echo "The variable does not equal 200."
fi
if [ $mynum -gt 200 ]
then
	echo "The variable is greater then 200."
else
	echo "The variable is equals or less then 200."
fi

if [ -f ./myfile ]
then
	echo "The file exists."
else
	echo "The file does not exist."
fi

command=htop

if command -v $command
then
	echo "$command is avaliable, let's run it..."
else
	echo "$command is NOT avaliable, installing it..."
	sudo apt update && sudo apt install -y $command
fi

$command

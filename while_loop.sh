#!/bin/bash

filename=testfile

while [ -f $filename ]
do
	echo "The $filename file exists, as of $(date)" 
	sleep 5
done

echo "The file $filename does not exist. Exiting..."


#!/bin/bash

local_bin=/usr/local/bin

if [ $# -eq 1 ]; then
	if [ -f $1 ]; then
		sudo cp $1 $local_bin/$1
		sudo chown root:root $local_bin/$1
		sudo chmod 755 $local_bin/$1
	else "File does not exist"
	fi
else
	echo "You must provide exactly one argument"
fi


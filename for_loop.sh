#!/bin/bash

if [[ -d logfiles && "$(find logfiles/*.log 2> /dev/null)" ]]; then
for file in logfiles/*.log
do
	tar -czvf $file.tar.gz $file
done

else
	echo "The directory 'logfiles' does not exist or it's empty!"
	exit 1
fi


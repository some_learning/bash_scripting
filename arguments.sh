#!/bin/bash

error(){
	echo $@
	echo "Please try again"
	exit 1
}

[ $# -eq 1 ] || error "This script requires exactly one directory path passed to it."

[ -d $1 ] || error "The directory does not exist!"

lines=$(ls -lh $1 | wc -l)

echo "You have $(($lines-1)) objects in the $1 directory."


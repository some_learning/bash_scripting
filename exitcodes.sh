#!/bin/bash

directory=/etccc
string="The directory $directory"

if [ -d $directory ]
then
	echo "$string exists."
	exit 0
else
	echo "$string doesn't exist."
	exit 199
fi

echo "The exit code for this script run is: $?"
echo "You didn't see that statement."
echo "You won't see this one either."


#!/bin/bash

backup_list=$1
dest=$2
logs=$dest/logs
error(){
	while [ $# -gt 1 ];
	do
		echo $1
		shift
	done
	exit $1
}
# Check to make sure the user has entered exactly two arguments.

[ $# -eq 2 ] || error 'Usage: backup.sh <source_directory> <target_directory>' 'please try again.' 1

[ $(command -v rsync) ] || error 'This script requires rsync to be installed.' 'Please use your distributions package manager to install it and try again.' 2

# Capture the current date, and store it in the ISO-8601 format
current_date=$(date -I)


# -a -archive mode(copy timestamp, mode etc.)
# -v verbose, -b create backups
# --delete (delete files which were deleted from source, but present in dest)
rsync_options="-avbr --backup-dir $dest/$current_date --delete"

backup() {
	$(which rsync) $rsync_options $1 $dest/current >> $logs/$current_date.log 2>>$logs/error_$current_date.log
}

for dir in $(cat $backup_list) ; do
	backup $HOME/$dir
done

